﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(EnterpriseAPI.Startup))]

namespace EnterpriseAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            // Serviços e configuração da API da Web
            var config = new HttpConfiguration();
            // Rotas da API da Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{version}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }

            );

            ActivateAcessTokkens(app);

            app.UseWebApi(config);
        }

        private void ActivateAcessTokkens(IAppBuilder app)
        {
            var TokenConfigurationOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(1),
                Provider = new AccessTokenProvider()
            };

            app.UseOAuthAuthorizationServer(TokenConfigurationOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }
    }
}
