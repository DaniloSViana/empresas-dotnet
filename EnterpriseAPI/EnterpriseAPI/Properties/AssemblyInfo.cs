﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As Informações Gerais sobre um assembly são controladas por meio do 
// conjunto de atributos a seguir. Altere esses valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("EnterpriseAPI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("EnterpriseAPI")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Configurar o ComVisible como false torna os tipos desse assembly invisíveis 
// para componentes COM.  Se for necessário acessar um tipo nesse assembly a partir do 
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// A GUID a seguir será referente à ID do typelib se este projeto for exposto ao COM
[assembly: Guid("80502410-adee-47ba-8a8c-6fa1cb00e033")]

// As informações de versão de um assembly consistem nos quatro valores a seguir:
//
//      Versão Principal
//      Versão Secundária 
//      Número da Versão
//      Revisão
//
// É possível especificar todos os valores ou utilizar como padrão os Números de Revisão e da Versão 
// usando o '*' como mostrado abaixo:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
