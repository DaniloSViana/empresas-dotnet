﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace EnterpriseAPI.Models
{
    public class Enterprise_Type
    {
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
        public List<Enterprise_Type> GravarEnterpriseTypes()
        {
            var pathArquivo = HostingEnvironment.MapPath(@"~/App_Data\enterprise_types.json");

            var json = System.IO.File.ReadAllText(pathArquivo);

            var listaEnterpriseTypes = JsonConvert.DeserializeObject<List<Enterprise_Type>>(json);
            var listaEnterpriseTypesGravadas = new List<Enterprise_Type>();

            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            IDbConnection connection;
            connection = new SqlConnection(connectionString);

            connection.Open();
            IDbCommand insertCmd = connection.CreateCommand();

            try
            {
                foreach (Enterprise_Type enterprise_type in listaEnterpriseTypes)
                {
                    insertCmd.Parameters.Clear();
                    insertCmd.CommandText = @"insert into Enterprise_types (Id, Enterprise_type_name) " +
                                                            @" values (@Id, @Enterprise_type_name)";

                    IDbDataParameter pId = new SqlParameter("Id", enterprise_type.id);
                    insertCmd.Parameters.Add(pId);
                    IDbDataParameter pEnterprise_name = new SqlParameter("Enterprise_type_name", enterprise_type.enterprise_type_name);
                    insertCmd.Parameters.Add(pEnterprise_name);
               

                    insertCmd.ExecuteNonQuery();

                    listaEnterpriseTypesGravadas.Add(enterprise_type);
                }

                connection.Close(); return listaEnterpriseTypesGravadas;
            }
            catch (Exception ex) { connection.Close(); return null; }

        }
    
        public Enterprise_Type ObterPorId(int id)
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            IDbConnection connection;
            connection = new SqlConnection(connectionString);
            connection.Open();
            IDbCommand selectCmd = connection.CreateCommand();
            selectCmd.CommandText = @"select * from Enterprise_types where Id = " + id;
            IDataReader result = selectCmd.ExecuteReader();

            Enterprise_Type enterprise_type = new Enterprise_Type();
            while (result.Read())
            {
                enterprise_type.id = Convert.ToInt32(result["Id"]);
                enterprise_type.enterprise_type_name = result["Enterprise_type_name"].ToString();

            }
            connection.Close();

            return enterprise_type;
        }
    }
}