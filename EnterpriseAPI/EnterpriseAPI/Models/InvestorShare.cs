﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseAPI.Models
{
    public class InvestorShare
    {
        public int IdInvestor { get; set; }
        public int? IdEnterprise { get; set; }
        public decimal Shares_owned { get; set; }
              
    }
}