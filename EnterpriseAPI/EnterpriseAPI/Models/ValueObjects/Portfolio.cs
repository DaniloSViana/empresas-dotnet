﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseAPI.Models.ValueObjects
{
    public class Portfolio
    {
        public int Enterprises_number { get { return Enterprises != null? Enterprises.Count() : 0; } }

        public ICollection<Enterprise> Enterprises { get; set;}

    }
}