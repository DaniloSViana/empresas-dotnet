﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Web;
using System.Web.Hosting;

namespace EnterpriseAPI.Models
{
    public class Enterprise
    {
        #region Propriedades
        public int Id { get; set; }
        public string Email_enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        virtual public bool Own_enterprise { get; set; }
        public string Enterprise_name { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Value { get; set; }
        public decimal Share_price { get; set; }

        [JsonIgnore]
        public int? IdEnterprise_Type { get; set; }   public virtual Enterprise_Type Enterprise_type { get; set; }
        
        #endregion

        #region Métodos
        public List<Enterprise> ListarEnterprises()
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            IDbConnection connection;
            connection = new SqlConnection(connectionString);
            connection.Open();
            IDbCommand selectCmd = connection.CreateCommand();
            selectCmd.CommandText = "select * from Enterprises";
            IDataReader result = selectCmd.ExecuteReader();

            List<Enterprise> enterprises = new List<Enterprise>();
            while (result.Read())
            {
                Enterprise ent = new Enterprise();
                ent.Id = Convert.ToInt32(result["Id"]);
                ent.Enterprise_name = result["Enterprise_name"].ToString();
                ent.City = result["City"].ToString();
                ent.Country = result["Country"].ToString();
                ent.Photo = result["Photo"].ToString();
                ent.IdEnterprise_Type = Convert.ToInt32(result["Id_enterprise_type"]);
                var enterprise_type = new Enterprise_Type();
                ent.Enterprise_type = enterprise_type.ObterPorId(Convert.ToInt32((ent.IdEnterprise_Type != null ? ent.IdEnterprise_Type : 0)));

                enterprises.Add(ent);
            }
            connection.Close();

            return enterprises;
        }

        public Enterprise ObterEnterprisePorIdEDescricao(int id, string descricao)
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            IDbConnection connection;
            connection = new SqlConnection(connectionString);
            connection.Open();
            IDbCommand selectCmd = connection.CreateCommand();
            selectCmd.CommandText = @"select * from Enterprises where Id_enterprise_type = " + id + " and Description like %'" + descricao + "'";
            IDataReader result = selectCmd.ExecuteReader();

            Enterprise enterprise = new Enterprise();
            while (result.Read())
            {
                enterprise.Id = Convert.ToInt32(result["Id"]);
                enterprise.Enterprise_name = result["Enterprise_name"].ToString();
                enterprise.Email_enterprise = result["Email"].ToString();
                enterprise.IdEnterprise_Type = Convert.ToInt32(result["Id_enterprise_type"]);
                enterprise.City = result["City"].ToString();
                enterprise.Country = result["Country"].ToString();
                enterprise.Photo = result["Photo"].ToString();

            }
            connection.Close();

            return enterprise;
        }

        
        public List<Enterprise> GravarEnterprises()
        {
            var pathArquivo = HostingEnvironment.MapPath(@"~/App_Data\enterprises.json");

            var json = System.IO.File.ReadAllText(pathArquivo);

            var listaEnterprises = JsonConvert.DeserializeObject<List<Enterprise>>(json);
            var listaEnterprisesGravadas = new List<Enterprise>();

            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            IDbConnection connection;
            connection = new SqlConnection(connectionString);

            connection.Open();
            IDbCommand insertCmd = connection.CreateCommand();
            
            try
            {
                foreach(Enterprise enterprise in listaEnterprises)
                {
                    insertCmd.Parameters.Clear();
                    insertCmd.CommandText = @"insert into Enterprises (Enterprise_name, Description, City, Country, Id_enterprise_type) " +
                                                            @" values (@Enterprise_name, @Description, @City, @Country, @IdEnterprise_Type)";

                    
                    IDbDataParameter pEnterprise_name = new SqlParameter("Enterprise_name", enterprise.Enterprise_name);
                    insertCmd.Parameters.Add(pEnterprise_name);
                    IDbDataParameter pDescription  = new SqlParameter("Description", enterprise.Description);
                    insertCmd.Parameters.Add(pDescription);
                    IDbDataParameter pCity = new SqlParameter("City", enterprise.City);
                    insertCmd.Parameters.Add(pCity);
                    IDbDataParameter pCountry = new SqlParameter("Country", enterprise.Country);
                    insertCmd.Parameters.Add(pCountry);
                    IDbDataParameter pIdEnterprise_Type = new SqlParameter("IdEnterprise_Type", enterprise.Enterprise_type.id);
                    insertCmd.Parameters.Add(pIdEnterprise_Type);

                    insertCmd.ExecuteNonQuery();
                   
                    listaEnterprisesGravadas.Add(enterprise);
                }

                connection.Close(); return listaEnterprisesGravadas;
            }
            catch(Exception ex) { connection.Close();  return null; }
            
        }

        #endregion

    }
}