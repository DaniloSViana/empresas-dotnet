﻿using EnterpriseAPI.Models.ValueObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EnterpriseAPI.Models
{
    public class Investor
    {
        public int Id { get; set; }
        public string Investor_name { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }

        // Após construir objeto investor, obter do bando de dados a lista de InvestorShares e 
        // foreach distinct IdEnterprise in Shares.IdEnterprise, Portfolio.Enterprises.Add(ObterEnterprisePorId(IdEnterprise));
        public virtual Portfolio Portfolio { get; set; }
        public virtual decimal Portfolio_value { get { return (Shares != null? Shares.Sum(s =>s.Shares_owned):0); } }
        public virtual bool First_access { get; set; }
        public bool Super_angel { get; set; }

        [JsonIgnore]
        public virtual ICollection<InvestorShare> Shares { get; set; }

       
        public Investor ObterPorEmailPassword(string email, string password)
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            Investor investor = new Investor();
            bool success = false;
            try
            {
                IDbConnection connection;
                connection = new SqlConnection(connectionString);
                connection.Open();
                IDbCommand selectCmd = connection.CreateCommand();
                selectCmd.CommandText = @"select * from Investors where Email = '" + email + "' and Password = '" + password + "'";
                IDataReader result = selectCmd.ExecuteReader();
                               
                while (result.Read())
                {
                    investor.Id = Convert.ToInt32(result["Id"]);
                    investor.Investor_name = result["Investor_name"].ToString();
                    investor.Email = result["Email"].ToString();
                    investor.Password = result["Password"].ToString();
                    investor.City = result["City"].ToString();
                    investor.Country = result["Country"].ToString();
                    investor.Balance = Convert.ToDecimal(result["Balance"]);
                    investor.Photo = result["Photo"].ToString();
                    investor.Portfolio = new Portfolio();
                    investor.First_access = Convert.ToBoolean(result["First_access"]);
                    investor.Super_angel = Convert.ToBoolean(result["Super_angel"]);
                    success = true;
    
            }
                connection.Close();

            }
            
            catch
            {
                success = false;
            }
            
            return success? investor: null;
        }
        public List<Investor> ListarInvestors()
        {
            string connectionString = ConfigurationManager.AppSettings["ConnetcionString"];

            IDbConnection connection;
            connection = new SqlConnection(connectionString);
            connection = new SqlConnection(connectionString);
            connection.Open();
            IDbCommand selectCmd = connection.CreateCommand();
            selectCmd.CommandText = "select * from Investors";
            IDataReader result = selectCmd.ExecuteReader();

            List<Investor> investors = new List<Investor>();
            while (result.Read())
            {
                Investor inv = new Investor();
                inv.Id = Convert.ToInt32(result["Id"]);
                inv.Investor_name = result["Investor_name"].ToString();
                inv.Email = result["Email"].ToString();
                inv.City = result["City"].ToString();
                inv.Country = result["Country"].ToString();
                inv.Balance = Convert.ToDecimal(result["Balance"]);                
                inv.Photo = result["Photo"].ToString();
                inv.Portfolio = new Portfolio();
                inv.First_access = false;//tratar autenticação
                inv.Super_angel = Convert.ToBoolean(result["Super_angel"]);

                investors.Add(inv);
            }
            connection.Close();

            return investors;
        }

    }


}