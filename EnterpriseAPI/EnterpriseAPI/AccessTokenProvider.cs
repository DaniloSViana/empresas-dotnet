﻿using EnterpriseAPI.Models;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EnterpriseAPI
{
    public class AccessTokenProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            Investor investor = new Investor();
            investor = investor.ObterPorEmailPassword(context.UserName, context.Password);

            if (investor == null)
            {
                context.SetError("invalid_grant", "Investidor não foi cadastrado ou a senha incorreta");
            }

            var identidadeInvestidor = new ClaimsIdentity(context.Options.AuthenticationType);
            context.Validated(identidadeInvestidor);
            

        }

    }
   
}