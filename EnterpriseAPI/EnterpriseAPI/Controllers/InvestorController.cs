﻿using EnterpriseAPI.Models;
using EnterpriseAPI.Models.ValueObjects;
using System.Collections.Generic;
using System.Web.Http;

namespace EnterpriseAPI.Controllers
{
    [RoutePrefix("api/{version}/users/auth")]
    public class InvestorController : ApiController
    {
        // GET: api/Investor
        public IEnumerable<Investor> Get()
        {
            Investor investor = new Investor();
            investor.Portfolio = new Portfolio() { Enterprises = new List<Enterprise>()};
            List<Investor> investors = new List<Investor>();
            investors.Add(investor);
           return investors;
        }

        // Post: api/Investor
        [HttpPost]
        [Route("sign_in")]
         
        public Investor Signin([FromBody] User user)
        {
            Investor investor = new Investor();
            investor = investor.ObterPorEmailPassword(user.Email, user.Password);

            return investor;
            //acrescentar enterprise e success
        }

      
        // PUT: api/Investor/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Investor/5
        public void Delete(int id)
        {
        }
    }
}
