﻿using EnterpriseAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace EnterpriseAPI.Controllers
{
    [RoutePrefix("api/{version}/configurar")]
    public class EnterprisesController : ApiController
    {
        
        // GET: api/Enterprise
        public IEnumerable<Enterprise> Get()
        {
            Enterprise enterprise = new Enterprise();

            return enterprise.ListarEnterprises();
        }

        // GET: api/Enterprise
        [HttpGet]
        [Route("gravarEnterprises")]

        public IEnumerable<Enterprise> Gravar()
        {
            Enterprise enterprise = new Enterprise();
            Enterprise_Type enterprise_type = new Enterprise_Type();
            enterprise_type.GravarEnterpriseTypes();
            return enterprise.GravarEnterprises();
        }

        // GET: api/Enterprises/5
      
        public Enterprise Get(int id)
        {
            Enterprise enterprise = new Enterprise();
            try
            {
                return enterprise.ListarEnterprises().Where(x => x.Id == id).FirstOrDefault();
            }
            catch(Exception)
            { return null; }
            
        }

        
        public IEnumerable<Enterprise> Get(int enterprise_types, string name)
        {
            List<Enterprise> resultado = new List<Enterprise>();
            Enterprise enterprise = new Enterprise();
            try
            {
                resultado = enterprise.ListarEnterprises().Where(x => x.IdEnterprise_Type == enterprise_types
                                                         && x.Enterprise_name.ToLower().Contains(name.ToLower())).ToList();
                return resultado;
            }
            catch (Exception ex)
            { return null; }

        }

        // POST: api/Enterprises
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Enterprises/5
        public void Put(int Id, [FromBody]string value)
        {
        }

        // DELETE: api/Enterprises/5
        public void Delete(int Id)
        {
        }
    }
}
