﻿CREATE TABLE [dbo].[Enterprises] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [Email_enterprise]   NVARCHAR (50)  NULL,
    [Facebook]           NVARCHAR (50)  NULL,
    [Twitter]            NVARCHAR (50)  NULL,
    [Linkedin]           NVARCHAR (50)  NULL,
    [Phone]              NVARCHAR (50)  NULL,
    [Enterprise_name]    NVARCHAR (50)  NULL,
    [Photo]              NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (MAX) NULL,
    [City]               NVARCHAR (50)  NULL,
    [Country]            NVARCHAR (50)  NULL,
    [Value]              DECIMAL (18)   NULL,
    [Share_price]        DECIMAL (18)   NULL,
    [Id_enterprise_type] INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Investors] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [Investor_name] NVARCHAR (50) NULL,
    [Email]         NVARCHAR (50) NULL,
	[Password]      NVARCHAR (59) NULL,
    [City]          NVARCHAR (50) NULL,
    [Country]       NVARCHAR (50) NULL,
    [Balance]       DECIMAL (18)  NULL,
    [Photo]         NVARCHAR (50) NULL,
	[First_access]  BIT           NULL,
    [Super_angel]   BIT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Enterprise_types] (
    [Id]                   INT           NOT NULL,
    [Enterprise_type_name] NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


